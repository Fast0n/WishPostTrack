import json
import requests
import sys
from googletrans import Translator
translator = Translator()

URL = "https://wishpost.wish.com/api/tracking/search"

data = {
    "X_XSRFToken": "null",
    "ids[]": sys.argv[1],
    "statetoken": "eyJqYXIiOnsidmVyc2lvbiI6InRvdWdoLWNvb2tpZUAyLjMuMSIsInN0b3JlVHlwZSI6Ik1lbW9yeUNvb2tpZVN0b3JlIiwicmVqZWN0UHVibGljU3VmZml4ZXMiOnRydWUsImNvb2tpZXMiOlt7InZhbHVlIjoiNDZlMDBkOWRhOGJkNDRjZTllMjRhZmI1YzBhMzc3MjciLCJleHBpcmVzIjoiMjAyMy0wNC0wM1QwOTozODo0Mi42MzRaIiwiaHR0cE9ubHkiOmZhbHNlLCJzZWN1cmUiOmZhbHNlLCJrZXkiOiJic2lkIiwibWF4QWdlIjoxNTc3NjY0MDAsImRvbWFpbiI6Indpc2guY29tIiwicGF0aCI6Ii8iLCJob3N0T25seSI6ZmFsc2UsImNyZWF0aW9uIjoiMjAxOC0wNC0wM1QwOTozODo0Mi42MzRaIiwibGFzdEFjY2Vzc2VkIjoiMjAxOC0wNC0wM1QwOTozODo0Mi42MzRaIn0seyJ2YWx1ZSI6IjE1MjI3NDgxNzUiLCJleHBpcmVzIjoiMjAyMy0wNC0wM1QwOTozODo0Mi42NDZaIiwiaHR0cE9ubHkiOmZhbHNlLCJzZWN1cmUiOmZhbHNlLCJrZXkiOiJIbV9sdnRfYmRkNjFhMTY1MGQyZThiZmRhZjhmNDEyYTA3YTUxYzkiLCJtYXhBZ2UiOjE1Nzc2NjQwMCwiZG9tYWluIjoid2lzaC5jb20iLCJwYXRoIjoiLyIsImhvc3RPbmx5IjpmYWxzZSwiY3JlYXRpb24iOiIyMDE4LTA0LTAzVDA5OjM4OjQyLjY0NloiLCJsYXN0QWNjZXNzZWQiOiIyMDE4LTA0LTAzVDA5OjM4OjQyLjY0NloifSx7InZhbHVlIjoiMTUyMjc0ODE3NSIsImV4cGlyZXMiOiIyMDIzLTA0LTAzVDA5OjM4OjQyLjY0OFoiLCJodHRwT25seSI6ZmFsc2UsInNlY3VyZSI6ZmFsc2UsImtleSI6IkhtX2xwdnRfYmRkNjFhMTY1MGQyZThiZmRhZjhmNDEyYTA3YTUxYzkiLCJtYXhBZ2UiOjE1Nzc2NjQwMCwiZG9tYWluIjoid2lzaC5jb20iLCJwYXRoIjoiLyIsImhvc3RPbmx5IjpmYWxzZSwiY3JlYXRpb24iOiIyMDE4LTA0LTAzVDA5OjM4OjQyLjY0OFoiLCJsYXN0QWNjZXNzZWQiOiIyMDE4LTA0LTAzVDA5OjM4OjQyLjY0OFoifV19fQ=="
}

r = requests.post(url=URL, data=data)
file = open("file.json", "w")
file.write(r.text)
file.close()

with open("file.json") as json_file:
    json_data = json.load(json_file)
    for key in json_data["data"][sys.argv[1]]["checkpoints"]:
        try:
            if key["remark"] != "":
                print("🚚 " + translator.translate(key["status_desc"], src='zh-CN', dest='it').text + "\n📍 " +
                      (translator.translate(key["remark"][3:], src='zh-CN', dest='en').text).upper() + "\n🕕 " + key["date"][:-3] + "\n\n")
            else:
                print("🚚 " + translator.translate(
                    key["status_desc"], src='zh-CN', dest='it').text + "\n🕕 " + key["date"][:-3] + "\n\n")
        except:
            print ("Errore!")
